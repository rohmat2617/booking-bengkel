package com.bengkel.booking.models;

import java.util.List;

import com.bengkel.booking.interfaces.IBengkelPayment;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@AllArgsConstructor
@Getter
@Setter
public class BookingOrder implements IBengkelPayment {
    private static int generateId = 1;
    private String bookingId;
    private Customer customer;
    private List<ItemService> services;
    private String paymentMethod;
    private double totalServicePrice;
    private double totalPayment;

    @Builder
    public BookingOrder(Customer customer, List<ItemService> services, String paymentMethod) {
        this.bookingId = generateReservationId();
        this.customer = customer;
        this.services = services;
        this.paymentMethod = paymentMethod;
        this.totalServicePrice = calculateTotalServicePrice();
        this.totalPayment = calculateTotalPayment();
    }

    private synchronized String generateReservationId() {
        String generateReservationId = "Book-" + String.format("%02d", generateId);
        generateId++;
        return generateReservationId;
    }

    private double calculateTotalServicePrice() {
            totalServicePrice += totalServicePrice;
        return totalServicePrice; 
    }

    private double calculateTotalPayment() {
     
        return 0.0;
    }

    @Override
    public void calculatePayment() {
        double discount = 0;
        if (paymentMethod.equalsIgnoreCase("Saldo Coin")) {
            discount = getTotalServicePrice() * RATES_DISCOUNT_SALDO_COIN;
        } else {
            discount = getTotalServicePrice() * RATES_DISCOUNT_CASH;
        }
        setTotalPayment(getTotalServicePrice() - discount);
    }
}

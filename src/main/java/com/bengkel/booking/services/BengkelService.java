package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;

public class BengkelService {

	// Silahkan tambahkan fitur-fitur utama aplikasi disini

	public static String[] credentials;

	// Login
	public static void getLogin() {
		Scanner input = new Scanner(System.in);

		PrintService.getPrintFormLogin();
		for (int i = 0; i < 3; i++) {
			credentials = Validation.getValidInputLogin(input);

			Customer customerid = Validation.getValidCustomerIdByListId(MenuService.listAllCustomers, credentials[0]);
			Customer password = Validation.getValidPasswordIdByListId(MenuService.listAllCustomers, credentials[1],
					credentials[0]);

			if (customerid != null && password != null) {
				MenuService.mainMenu();
				break;
			} else {

				System.out.println(" Attempt " + (i + 1) + " failed.");
				System.out.println("==================================");
			}

			if (i == 2) {
				PrintService.getNotif();
				System.out.println(" Maximum attempts reached. \n Exiting application.");
				System.out.println("==================================");
				System.exit(0);
			}
		}
	}

	// Info Customer
	public static void getPrintCustomerInformation(List<Customer> listAllCustomers, String inputcustomerId) {
		String line = "+-------------------------------------------------------------------------------------------------+";
		System.out.println(line);
		for (Customer customer : listAllCustomers) {
			if (customer.getCustomerId().equals(inputcustomerId)) {

				if (customer instanceof MemberCustomer) {
					System.out.println(" Membership");

				} else {
					System.out.println(" Non-Member");
				}
				System.out.println(line);
				System.out.println("\t\t\t\t\t Customer Profile");
				System.out.println("Costumer Id\t: " + customer.getCustomerId());
				System.out.println("Name\t\t: " + customer.getName());

				if (customer instanceof MemberCustomer) {
					System.out.println("Costumer Status : Membership");

				} else {
					System.out.println("Costumer Status : Non-Member");
				}
				System.out.println("Alamat\t\t: " + customer.getAddress());

				if (customer instanceof MemberCustomer) {
					MemberCustomer memberCustomer = (MemberCustomer) customer;
					System.out.println("Saldo Koin\t: " + memberCustomer.getSaldoCoin());

				}
				System.out.println(line);
				System.out.println("\t\t\t\t\t\t Kendaraan");
				System.out.println(line);
				PrintService.printVechicle(customer.getVehicles());

				System.out.println("==================================");
				return;
			}
		}
	}
	// Booking atau Reservation

	public static void getBooking(List<Customer> listAllCustomers, List<ItemService> listItemService,
			List<BookingOrder> listBookingOrders,
			String inputcustomerId) {
		String inputVehicleId = "";
		String inputPaymentMethod = "";
		double servicePrice = 0;
		Scanner input = new Scanner(System.in);
		inputVehicleId = Validation.getValidInputVehicle(input);

		String vehicleType = Validation.getValidVehicleTypeByListId(listAllCustomers, inputVehicleId);

		if (vehicleType != null) {
			PrintService.printService(MenuService.listAllItemService, vehicleType);
		}

		ArrayList<String> serviceIds = Validation.getValidServisByListId(listItemService, listAllCustomers, input,
				BengkelService.credentials[0]);

		inputPaymentMethod = Validation.getPaymentMethod(listAllCustomers, listItemService, serviceIds, input,
				inputcustomerId);

		List<ItemService> selectedServices = new ArrayList<>();
		for (String serviceId : serviceIds) {
			Optional<ItemService> optionalService = Validation.findServiceById(listItemService, serviceId);
			if (optionalService.isPresent()) {
				ItemService itemService = optionalService.get();
				selectedServices.add(itemService);
				System.out.println("Service Name: " + itemService.getServiceName());
			}
		}
		System.out.println(inputPaymentMethod);

		servicePrice = Validation.getTotalPaymentCustomer(listAllCustomers, listItemService, serviceIds,
				inputPaymentMethod);

		Customer customerPerson = null;
		for (Customer person : MenuService.listAllCustomers) {
			if (person.getCustomerId().equals(inputcustomerId)) {
				customerPerson = person;
				break;
			}
		}

		BookingOrder newBooking = BookingOrder.builder()
				.customer(customerPerson)
				.services(selectedServices)
				.paymentMethod(inputPaymentMethod)
				.totalServicePrice(servicePrice)
				.build();
		listBookingOrders.add(newBooking);
	}

	// Top Up Saldo Coin Untuk Member Customer

	// Logout

}

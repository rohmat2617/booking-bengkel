package com.bengkel.booking.services;

import java.util.List;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Car;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.Vehicle;

public class PrintService {

	public static void printMenu(String[] listMenu, String title) {
		String line = "+---------------------------------+";
		int number = 1;
		String formatTable = " %-2s. %-25s %n";

		System.out.printf("%-25s %n", title);
		System.out.println(line);

		for (String data : listMenu) {
			if (number < listMenu.length) {
				System.out.printf(formatTable, number, data);
			} else {
				System.out.printf(formatTable, 0, data);
			}
			number++;
		}
		System.out.println(line);
		System.out.println();
	}

	public static void printVechicle(List<Vehicle> listVehicle) {
		String formatTable = "| %-2s | %-15s | %-10s | %-15s | %-15s | %-5s | %-15s |%n";
		String line = "+----+-----------------+------------+-----------------+-----------------+-------+-----------------+%n";
		System.out.format(line);
		System.out.format(formatTable, "No", "Vechicle Id", "Warna", "Brand", "Transmisi", "Tahun", "Tipe Kendaraan");
		System.out.format(line);
		int number = 1;
		String vehicleType = "";
		for (Vehicle vehicle : listVehicle) {
			if (vehicle instanceof Car) {
				vehicleType = "Mobil";
			} else {
				vehicleType = "Motor";
			}
			System.out.format(formatTable, number, vehicle.getVehiclesId(), vehicle.getColor(), vehicle.getBrand(),
					vehicle.getTransmisionType(), vehicle.getYearRelease(), vehicleType);
			number++;
		}
		System.out.printf(line);
	}

	public static void getPrintFormLogin() {
		System.out.println("==================================");
		System.out.printf("| %-30s |\n", "Welcome To The Booking Bengkel");
		System.out.printf("| %-30s |\n", "           Form Login");
		System.out.println("==================================");

	}

	public static void getNotif() {
		System.out.println("==================================");
		System.out.printf("| %-25s |\n", "\t   Notifikasi");
		System.out.println("==================================");

	}

	public static void printService(List<ItemService> listItemService, String vehicleType) {
		String formatTable = "| %-2s | %-15s | %-20s | %-15s | %-15s |%n";
		String line = "+----+-----------------+----------------------+-----------------+-----------------+%n";
		System.out.format(line);
		System.out.format(formatTable, "No", "Service id", "Nama Service", "Tipe Kendaraan", "Harga");
		System.out.format(line);
		int number = 1;

		for (ItemService service : listItemService) {
			if (service.getVehicleType().equalsIgnoreCase(vehicleType)) {
				System.out.format(formatTable, number, service.getServiceId(), service.getServiceName(),
						service.getVehicleType(), service.getPrice());
				number++;
			}
		}
		System.out.printf(line);
	}

	public static void printBooking(List<BookingOrder> listBookingOrders) {
		String formatTable = "| %-2s | %-15s | %-20s | %-15s | %-15s | %-15s | %-25s |%n";
		String line = "+----+-----------------+----------------------+-----------------+-----------------+---------------------------+%n";
		System.out.format(line);
		System.out.format(formatTable, "No", "Booking id", "Nama Customer", "Payment Method", "Total Service",
				"Total Payment", "List Service");
		System.out.format(line);
		int number = 1;

		for (BookingOrder booking : listBookingOrders) {
			System.out.format(formatTable, number,
					booking.getBookingId(),
					booking.getCustomer().getName(),
					booking.getPaymentMethod(),
					getPrice(booking.getServices()),
					booking.getTotalPayment(),
					getServiceNames(booking.getServices()));
			number++;
		}
		System.out.printf(line);
	}

	private static String getServiceNames(List<ItemService> services) {
		StringBuilder serviceNames = new StringBuilder("[");
		for (ItemService service : services) {
			serviceNames.append(service.getServiceName()).append(", ");
		}

		if (!services.isEmpty()) {
			serviceNames.delete(serviceNames.length() - 2, serviceNames.length());
		}
		serviceNames.append("]");
		return serviceNames.toString();
	}

	private static String getPrice(List<ItemService> services) {
		StringBuilder serviceNames = new StringBuilder("[");
		for (ItemService service : services) {
			serviceNames.append(service.getPrice()).append(", ");

		}

		if (!services.isEmpty()) {
			serviceNames.delete(serviceNames.length() - 2, serviceNames.length());
		}
		serviceNames.append("]");
		return serviceNames.toString();
	}

}
// Silahkan Tambahkan function print sesuai dengan kebutuhan.

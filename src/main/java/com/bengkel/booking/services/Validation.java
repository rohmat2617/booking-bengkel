package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;

import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;

public class Validation {

	public static String validasiInput(String question, String errorMessage, String regex) {
		Scanner input = new Scanner(System.in);
		String result;
		boolean isLooping = true;
		do {
			System.out.print(question);
			result = input.nextLine();

			// validasi menggunakan matches
			if (result.matches(regex)) {
				isLooping = false;
			} else {
				System.out.println(errorMessage);
			}

		} while (isLooping);

		return result;
	}

	public static int validasiNumberWithRange(String question, String errorMessage, String regex, int max, int min) {
		int result;
		boolean isLooping = true;
		do {
			result = Integer.valueOf(validasiInput(question, errorMessage, regex));
			if (result >= min && result <= max) {
				isLooping = false;
			} else {
				System.out.println("Pilihan angka " + min + " s.d " + max);
			}
		} while (isLooping);

		return result;
	}

	public static String[] getValidInputLogin(Scanner input) {
		String inputCustomerId;
		String inputPassword;
		boolean isCustomerIdValid;
		boolean isPasswordValid;

		do {
			System.out.printf(" %-12s ", "Customer Id :");
			inputCustomerId = input.nextLine();

			System.out.printf(" %-12s ", "Password    :");
			inputPassword = input.nextLine();
			System.out.println("==================================");

			isCustomerIdValid = !inputCustomerId.trim().isEmpty();
			isPasswordValid = !inputPassword.trim().isEmpty();

			if (!isCustomerIdValid || !isPasswordValid) {
				System.out.println(" Please enter, customerId, \n and PasswordId not empty.");
				System.out.println("==================================");

			}
		} while (!isCustomerIdValid || !isPasswordValid);

		return new String[] { inputCustomerId, inputPassword };
	}

	public static Customer getValidCustomerIdByListId(List<Customer> listAllCustomers, String inputCustomerId) {
		Optional<Customer> optionalCustomerId = findCustomerId(listAllCustomers, inputCustomerId);
		if (optionalCustomerId.isPresent()) {
			Customer customer = optionalCustomerId.get();
			PrintService.getNotif();
			System.out.println("----------------------------------");
			System.out.println("Customer ID Found");
			return customer;
		} else {
			PrintService.getNotif();
			System.out.println("----------------------------------");
			System.out.println("Customer ID, Not Found.");

			return null;
		}
	}

	public static Optional<Customer> findCustomerId(List<Customer> listAllCustomers, String inputCustomerId) {
		return listAllCustomers.stream().filter(employee -> employee.getCustomerId().equals(inputCustomerId))
				.findFirst();
	}

	public static List<Customer> getCustomerId(List<Customer> listAllCustomers, String inpuCustomerId) {
		return listAllCustomers.stream()
				.filter(customerid -> customerid.getCustomerId().equalsIgnoreCase(inpuCustomerId))
				.collect(Collectors.toList());
	}

	public static Customer getValidPasswordIdByListId(List<Customer> listAllCustomers, String inputPasswordId,
			String inputCustomerId) {
		Optional<Customer> optionalPasswordId = findPasswordId(listAllCustomers, inputPasswordId, inputCustomerId);
		if (optionalPasswordId.isPresent()) {
			Customer password = optionalPasswordId.get();
			System.out.println("Password ID Found");
			System.out.println("----------------------------------");
			return password;
		} else {
			System.out.println("Password ID, Not Found.");
			System.out.println("----------------------------------");
			return null;
		}
	}

	public static Optional<Customer> findPasswordId(List<Customer> listAllCustomers, String inputPasswordId,
			String inputCustomerId) {
		return listAllCustomers.stream()
				.filter(customer -> customer.getCustomerId().equals(inputCustomerId)
						&& customer.getPassword().equals(inputPasswordId))
				.findFirst();
	}

	public static String getValidInputVehicle(Scanner input) {
		String inputVehicleId;
		boolean isVehicleIdValid;
		do {
			System.out.printf(" %-12s ", "Vehicle Id :");
			inputVehicleId = input.nextLine();
			System.out.println("==================================");

			isVehicleIdValid = !inputVehicleId.trim().isEmpty();

			if (!isVehicleIdValid) {
				System.out.println(" Please enter Vehicle Id, not empty.");
				System.out.println("==================================");
			}
		} while (!isVehicleIdValid);
		return inputVehicleId;
	}

	public static String getValidVehicleTypeByListId(List<Customer> listAllCustomers, String inputVehicleId) {
		Optional<Customer> optionalCustomer = findVehicleById(listAllCustomers, inputVehicleId);

		if (optionalCustomer.isPresent()) {
			Customer foundCustomer = optionalCustomer.get();
			PrintService.getNotif();
			System.out.println("----------------------------------");
			System.out.println("Vehicle Found:" + foundCustomer.getVehicles().get(0).getVehicleType());

			if (!foundCustomer.getVehicles().isEmpty()) {

				return foundCustomer.getVehicles().get(0).getVehicleType();
			}
		} else {
			PrintService.getNotif();
			System.out.println("----------------------------------");
			System.out.println("Vehicle Not Found.");
			return null;
		}

		return null;
	}

	public static Optional<Customer> findVehicleById(List<Customer> listAllCustomers, String inputVehicleId) {
		return listAllCustomers.stream()
				.filter(customer -> customer.getVehicles().stream()
						.anyMatch(vehicle -> vehicle.getVehiclesId().equalsIgnoreCase(inputVehicleId)))
				.findFirst();
	}

	public static ArrayList<String> getValidServisByListId(List<ItemService> listItemService,
			List<Customer> listAllCustomers,
			Scanner input, String inputcustomerId) {
		ArrayList<String> serviceIds = new ArrayList<>();

		boolean shouldRepeat = true;

		do {
			System.out.print("Input The Service Id: ");
			String inputServiceId = input.nextLine();
			Optional<ItemService> optionalServiceId = findServiceById(listItemService, inputServiceId);

			if (optionalServiceId.isPresent()) {
				ItemService service = optionalServiceId.get();

				System.out.println("Id Found, The ID You Entered Is " + service.getServiceId());
				if (serviceIds.contains(inputServiceId)) {
					System.out.println("Service ID already entered. Please enter a different Service ID.");
				}

				serviceIds.add(service.getServiceId());

				boolean isMember = false;

				for (Customer customer : listAllCustomers) {
					if (customer.getCustomerId().equals(inputcustomerId)) {
						if (customer instanceof MemberCustomer) {
							isMember = true;
						}
					}
				}

				if (!isMember) {

					System.out.println("Non-members are not allowed to access this.");

					break;
				}

				if (shouldRepeat && isMember) {
					int repeatCountForMember = 1;
					while (repeatCountForMember < 2) {
						System.out.print("Do you want to enter another Service Id? (Y/T): ");
						String repeatInput = input.nextLine().toLowerCase();
						shouldRepeat = repeatInput.equals("y");
						if (shouldRepeat) {
							repeatCountForMember++;
						} else {
							break;
						}
					}

					if (repeatCountForMember > 2) {
						System.out.println("You have reached the maximum limit of 2 service entries for members.");
						break;
					}
				}

			} else {
				System.out.println("Service With ID " + inputServiceId + " Not Found.");
				shouldRepeat = true;
			}
		} while (shouldRepeat);

		return serviceIds;
	}

	public static Optional<ItemService> findServiceById(List<ItemService> listItemService, String inputServiceId) {
		return listItemService.stream().filter(service -> service.getServiceId().equalsIgnoreCase(inputServiceId))
				.findFirst();
	}

	public static String getPaymentMethod(List<Customer> listAllCustomers, List<ItemService> listItemService,
			List<String> serviceIds, Scanner input, String inputcustomerId) {

		System.out.println("Select payment method:");

		boolean isMember = false;
		for (Customer customer : listAllCustomers) {
			if (customer.getCustomerId().equals(inputcustomerId)) {
				if (customer instanceof MemberCustomer) {
					isMember = true;
					System.out.println("1. Cash");
					System.out.println("2. Saldo Coin");
				} else {
					System.out.println("1. Cash");
				}
			}
		}

		String selectedOption;
		do {
			System.out.print("Enter your choice: ");
			selectedOption = input.nextLine();

			if ((isMember && !selectedOption.equals("Cash") && !selectedOption.equals("Saldo Coin")) ||
					(!isMember && !selectedOption.equals("Cash"))) {
				System.out.println("Invalid option. Please enter 'Cash' or 'Saldo Coin'.");
			}

		} while ((isMember && !selectedOption.equals("Cash") && !selectedOption.equals("Saldo Coin"))
				|| (!isMember && !selectedOption.equals("Cash")));

		return selectedOption;
	}

	public static Double getTotalPaymentCustomer(List<Customer> listAllCustomers, List<ItemService> listItemService,
			List<String> serviceIds, String inputPaymentMethod) {

		double totalPayment = 0;
		double servicePrice = 0;
		for (String serviceId : serviceIds) {
			System.out.println("Service ID: " + serviceId);

			Optional<ItemService> optionalService = findServiceById(listItemService, serviceId);
			if (optionalService.isPresent()) {
				ItemService itemService = optionalService.get();
				servicePrice = itemService.getPrice();
				totalPayment += servicePrice;

				// Print the service price
				System.out.println("Service Price: " + servicePrice);
			} else {
				System.out.println("Service ID " + serviceId + " not found in the list of services.");
			}
		}

		return servicePrice;

	}
}

package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.repositories.CustomerRepository;
import com.bengkel.booking.repositories.ItemServiceRepository;

public class MenuService {
	public static List<Customer> listAllCustomers = CustomerRepository.getAllCustomer();
	public static List<ItemService> listAllItemService = ItemServiceRepository.getAllItemService();
	public static List<BookingOrder> listBookingOrders = new ArrayList<>();

	public static void run() {
		boolean isLooping = true;
		do {
			login();
			mainMenu();
		} while (isLooping);

	}

	public static void login() {
		BengkelService.getLogin();
	}

	public static void mainMenu() {
		String[] listMenu = { "Informasi Customer", "Booking Bengkel", "Top Up Bengkel Coin", "Informasi Booking",
				"Logout" };
		int menuChoice = 0;
		boolean isLooping = true;

		do {
			PrintService.printMenu(listMenu, "Booking Bengkel Menu");
			menuChoice = Validation.validasiNumberWithRange("Masukan Pilihan Menu:", "Input Harus Berupa Angka!",
					"^[0-9]+$", listMenu.length - 1, 0);
			System.out.println(menuChoice);

			switch (menuChoice) {
				case 1:
					BengkelService.getPrintCustomerInformation(
							Validation.getCustomerId(listAllCustomers, BengkelService.credentials[0]),
							BengkelService.credentials[0]);
					break;
				case 2:
					BengkelService.getBooking(listAllCustomers, listAllItemService,listBookingOrders, BengkelService.credentials[0]);
		
				PrintService.printBooking(listBookingOrders);
					break;
				case 3:
					// panggil fitur Top Up Saldo Coin
					break;
				case 4:
					// panggil fitur Informasi Booking Order
					break;
				default:
					System.out.println("Logout");
					isLooping = false;
					break;
			}
		} while (isLooping);

	}

	// Silahkan tambahkan kodingan untuk keperluan Menu Aplikasi
}
